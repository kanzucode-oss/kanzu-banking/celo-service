# Celo Service
Celo integration with Kanzu Banking to allow users to make use of the cryptocurrency(cUSD) and transact within the Celo Network

### Requirements
* Nodejs
* Nestjs

## Setup 
* Create database
```sql
CREATE DATABASE celo_service;
```
* Install sequelize-cli globally to make use of  handy commands that come with it.
```bash
npm install -g sequelize-cli
```
* Install nestjs/cli globally
```bash
npm install -g @nestjs/cli
```
* Install required dependencies 
```bash
npm install
```
* Create an env file. An env-sample is provided with the needed environment variables 

* Run migration 
```bash
sequelize db:migrate
```
* Run application 
```bash
npm start:dev
```
* Documentation
```bash
POST /api/v1/exchange - Convert UGX to USD to CUSD {data:{amount:  number}}
POST /api/v1/fsps- Create or find FSPs
GET /api/v1/fsps/:tenant_id- Get tenant by tenant_id
POST /api/v1/transfers - Send cUSD to FSP address {data:{amount:  number}}
```
The application will run on https://localhost:8000/api/v1