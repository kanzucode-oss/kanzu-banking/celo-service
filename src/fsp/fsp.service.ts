/* eslint-disable @typescript-eslint/no-var-requires */
import { Injectable, Inject, Request,HttpException,HttpStatus,Logger} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { FSP } from './fsp.model';
import {FSP_REPOSITORY} from './constants';
import { FspDto } from './dto/fsp.dto';

const Web3 = require('web3');
const ContractKit = require('@celo/contractkit')


@Injectable()
export class FspService {
    private readonly logger = new Logger(FspService.name);

    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    constructor(@Inject(FSP_REPOSITORY) private readonly fspRepository, private readonly configService: ConfigService) { }
    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    async create(@Request() req): Promise<FSP> {
        const network = this.configService.get<string>('NETWORK')
        const web3 = new Web3(network)
        const kit = ContractKit.newKitFromWeb3(web3)
        const account = kit.web3.eth.accounts.create()
        const address = account.address
        const privateKey = account.privateKey
        const tenant = req.get('Fineract-Platform-TenantId');
        const data = await this.fspRepository.findOrCreate({where: {tenant_id:tenant}, defaults:{
            address: address,
            private_key: privateKey
        }});
        const result = Object.assign({},data);
       return result;

    }
    async findTenant(tenant_id: string): Promise<FSP> {
        return await this.fspRepository.findOne({where: {tenant_id}})
    }

    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    async getBalance(tenant_id:string): Promise<FspDto> {
        const network = this.configService.get<string>('NETWORK')
        const web3 = new Web3(network)
        const kit = ContractKit.newKitFromWeb3(web3)
        const fsp = await this.findTenant(tenant_id)
        if(!fsp){
            this.logger.error(`${tenant_id} does not exist`);
            throw new HttpException(`${tenant_id} does not exist`, HttpStatus.FORBIDDEN);
        }
        const address = fsp.address
        const stabletoken = await kit.contracts.getStableToken()
        const conversion = 1000000000000000000;
        const cUSDBalance = await stabletoken.balanceOf(address);
        const newBalance = cUSDBalance / conversion;
        const data : FspDto = {
            tenant_id: tenant_id,
            balance: newBalance.toString(),
            address: fsp.address

        }
        return data

    }
}


