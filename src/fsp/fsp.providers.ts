import { FSP } from './fsp.model';
import { FSP_REPOSITORY } from './constants';

export const FspProvider = [
    {
        provide: FSP_REPOSITORY,
        useValue: FSP,
    },
];