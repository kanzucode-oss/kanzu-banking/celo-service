import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { ConfigModule } from '@nestjs/config';
import {FSP} from './fsp.model';
import { FspController } from './fsp.controller';
import { FspService } from './fsp.service';
import {FspProvider} from './fsp.providers';
@Module({
    imports: [SequelizeModule.forFeature([FSP]), ConfigModule],
    exports: [SequelizeModule],
    controllers: [FspController],
    providers: [FspService, ...FspProvider]
})
export class FspModule {}
