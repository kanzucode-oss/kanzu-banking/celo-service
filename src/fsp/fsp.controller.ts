/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Controller, UseGuards, Request, Post, Get, Param} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {FspService} from './fsp.service';
import { FSP } from './fsp.model';
import { FspDto } from './dto/fsp.dto';


@Controller('fsps')
export class FspController {
    constructor(private readonly fspService: FspService) { }

    @UseGuards(AuthGuard('bearer'))
    @Post()
    async create(@Request() req): Promise<FSP> {
        const tenant = await this.fspService.create(req);
        return tenant[0];

    }
    @UseGuards(AuthGuard('bearer'))
    @Get(':tenant_id')
    async getBalance(@Param() params): Promise<FspDto> {
        console.log(params.tenant_id)
        return await this.fspService.getBalance(params.tenant_id);
    }
}
