import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { FspProvider } from './fsp.providers';
import { FspService } from './fsp.service';

describe('FspService', () => {
  let service: FspService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports:[ConfigModule],
      providers: [FspService, ...FspProvider],
    }).compile();

    service = module.get<FspService>(FspService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
