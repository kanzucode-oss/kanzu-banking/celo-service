import { Column, Model, Table } from 'sequelize-typescript';

@Table
export class FSP extends Model<FSP> {
  @Column
  tenant_id: string;

  @Column
  address: string;

  @Column
  private_key: string;
}