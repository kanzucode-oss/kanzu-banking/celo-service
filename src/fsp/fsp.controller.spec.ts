import { ConfigModule } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { FspController } from './fsp.controller';
import { FspProvider } from './fsp.providers';
import { FspService } from './fsp.service';

describe('FspController', () => {
  let controller: FspController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports:[ConfigModule],
      controllers: [FspController],
      providers: [FspService, ...FspProvider]
    }).compile();

    controller = module.get<FspController>(FspController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
