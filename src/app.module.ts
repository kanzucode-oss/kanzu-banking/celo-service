import { Module, HttpModule } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FspModule } from './fsp/fsp.module';
import { AuthModule } from './auth/auth.module';
import { FSP } from './fsp/fsp.model';
import { ExchangeModule } from './exchange/exchange.module';
import { SendModule } from './send/send.module';
@Module({
  imports: [
    ConfigModule.forRoot(),
    SequelizeModule.forRoot({
      dialect: "mysql",
      host: process.env.DB_HOST,
      port: 3306,
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
      models: [FSP],
    }),
    FspModule,
    AuthModule,
    HttpModule,
    ExchangeModule,
    SendModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
