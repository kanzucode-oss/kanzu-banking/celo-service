import { Module } from '@nestjs/common';
import { SendService } from './send.service';
import { SendController } from './send.controller';
import { FspService } from '../fsp/fsp.service';
import { FspProvider } from '../fsp/fsp.providers';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports:[ConfigModule],
  providers: [SendService,FspService, ...FspProvider],
  controllers: [SendController]
})
export class SendModule {}

