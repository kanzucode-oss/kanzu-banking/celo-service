import { Body,Request,UseGuards, Controller, Post } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { SendDto } from './send.dto';
import { SendService } from './send.service';

@Controller('transfers')
export class SendController {
    constructor(private sendService: SendService){}
    @UseGuards(AuthGuard('bearer'))
    @Post()
    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    async create(@Request() req, @Body() data: SendDto):Promise<SendDto>{
        const amount = data.amount;
        const from = data.from;
        const to = data.to;
        const tenant_id = req.get('Fineract-Platform-TenantId');
        if(from == 'KanzuCode' && to == tenant_id) {
            return await this.sendService.send(tenant_id, amount)
        }
        if(from == tenant_id && to == 'KanzuCode') {
            return await this.sendService.liquidate(tenant_id, amount)
        }
    }
}
