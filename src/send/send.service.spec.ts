import { Test, TestingModule } from '@nestjs/testing';
import { SendService } from './send.service';
import { FspService } from '../fsp/fsp.service';
import { FspProvider } from '../fsp/fsp.providers';
import { ConfigModule } from '@nestjs/config';

describe('SendService', () => {
  let service: SendService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports:[ConfigModule],
      providers: [SendService, FspService, ...FspProvider],
    }).compile();

    service = module.get<SendService>(SendService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

