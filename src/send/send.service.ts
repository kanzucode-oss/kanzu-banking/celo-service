// eslint-disable-next-line @typescript-eslint/no-var-requires
import { Injectable,HttpException,HttpStatus, Logger} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { FspService } from '../fsp/fsp.service';
import { SendDto } from './send.dto';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const Web3 = require('web3');
// eslint-disable-next-line @typescript-eslint/no-var-requires
const ContractKit = require('@celo/contractkit')


@Injectable()
export class SendService {
    private readonly logger = new Logger(SendService.name);
    constructor(private fsp: FspService, private configService: ConfigService){}
    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    async send(tenant_id:string, amount:number): Promise<SendDto> {
        const network = this.configService.get<string>('NETWORK')
        const web3 = new Web3(network)
        const kit = ContractKit.newKitFromWeb3(web3)
    
        kit.connection.addAccount(process.env.KB_PRIVATE_KEY);
        const tenant = await this.fsp.findTenant(tenant_id);

        if(!tenant){
            this.logger.error(`${tenant_id} does not exist`);
            throw new HttpException(`${tenant_id} does not exist`, HttpStatus.FORBIDDEN);
        }
        const sender = process.env.KB_ADDRESS;
        const receiver = tenant.address;

        const amountWei = kit.web3.utils.toWei(amount.toString(), 'ether')
        const stabletoken = await kit.contracts.getStableToken()
        const senderBalanceBefore = await stabletoken.balanceOf(sender);
        if(amountWei > parseFloat(senderBalanceBefore.toString()) ){
            this.logger.error('Transfer value exceeded balance of sender');
            throw new HttpException(`Transfer value exceeded balance of sender`, HttpStatus.FORBIDDEN);
        }
        const cUSDtx = await stabletoken.transfer(receiver, amountWei).send({
            from: sender, 
            feeCurrency:stabletoken.address
        })
        const cUSDReceipt =  await cUSDtx.waitReceipt();
        this.logger.log('cUSD Transaction receipt: %o', cUSDReceipt)
        const cUSDBalance = await stabletoken.balanceOf(receiver);
        const senderBalance = await stabletoken.balanceOf(sender);
        this.logger.log(`FSP cUSD balance: ${cUSDBalance.toString()}`)
        this.logger.log(`KB previous balance: ${senderBalanceBefore.toString()}, KB current balance: ${senderBalance.toString()} `);


        const data: SendDto = {
            amount: amountWei,
            from: 'KanzuCode',
            to: tenant.tenant_id,
            status: 'Transfer Complete'
        }
        return data
    }
    async liquidate(tenant_id:string, amount:number): Promise<SendDto> {
        const network = this.configService.get<string>('NETWORK')
        const web3 = new Web3(network)
        const kit = ContractKit.newKitFromWeb3(web3)
    
        const tenant = await this.fsp.findTenant(tenant_id);
        kit.connection.addAccount(tenant.private_key);
        const sender = tenant.address;
        const receiver = process.env.KB_ADDRESS;
        const amountWei = kit.web3.utils.toWei(amount.toString(), 'ether')
        const stabletoken = await kit.contracts.getStableToken()
        const cUSDtx = await stabletoken.transfer(receiver, amountWei).send({
            from: sender, 
            feeCurrency:stabletoken.address
        })
        const cUSDReceipt =  await cUSDtx.waitReceipt();
        console.log('cUSD Transaction receipt: %o', cUSDReceipt)
        const cUSDBalance = await stabletoken.balanceOf(receiver);
        const senderBalance = await stabletoken.balanceOf(sender);
        console.log(`KB balance: ${cUSDBalance.toString()}`)
        console.log(`FSP balance: ${senderBalance.toString()}`)

        const data: SendDto = {
            amount: amountWei,
            from: tenant.tenant_id,
            to: 'KanzuCode',
            status: 'Transfer Complete'
        }
        return data
    }

}
