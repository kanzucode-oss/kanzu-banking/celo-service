import { Test, TestingModule } from '@nestjs/testing';
import { ConfigModule } from '@nestjs/config';
import { SendController } from './send.controller';
import {SendService} from './send.service';
import {FspService} from '../fsp/fsp.service';
import { FspProvider } from '../fsp/fsp.providers';

describe('SendController', () => {
  let controller: SendController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports:[ConfigModule],
      controllers: [SendController],
      providers:[SendService,FspService,...FspProvider]
    }).compile();

    controller = module.get<SendController>(SendController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

