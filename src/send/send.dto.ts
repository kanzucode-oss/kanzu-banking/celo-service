export class SendDto {
    amount: number;
    from: string;
    to: string;
    status: string;
}
