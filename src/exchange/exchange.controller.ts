import { Controller, Post, Body} from '@nestjs/common';
import { ExchangeDto } from './exchange.dto';
import {ExchangeService} from './exchange.service';
@Controller('exchange')
export class ExchangeController {
    constructor(private readonly exchangeService: ExchangeService) { }
    @Post()
    async create(@Body()  data: ExchangeDto, ): Promise<ExchangeDto> {
        const amount = data.amount
        const from = data.from
        const to = data.to
        return await this.exchangeService.exchange(amount, from, to)
    }
}
