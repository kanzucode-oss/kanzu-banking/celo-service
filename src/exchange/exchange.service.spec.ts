import { HttpModule } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { ExchangeService } from './exchange.service';

describe('ExchangeService', () => {
  let service: ExchangeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [ExchangeService],
    }).compile();

    service = module.get<ExchangeService>(ExchangeService);
  });

  it('should return converted amount', async () => {
    const result = {"from":"UGX", "to":"USD","amount": 14}
    jest.spyOn(service, 'exchange').mockImplementation(async () => result)
    const response = await service.exchange(50000, 'UGX','USD')
    expect(response['amount']).toEqual(result.amount);
  });

  it('should return UGX amount', async () => {
    const result = {"from":"USD", "to":"UGX","amount": 50000}
    jest.spyOn(service, 'exchange').mockImplementation(async () => result)
    const response = await service.exchange(14, 'USD','UGX')
    expect(response['amount']).toEqual(result.amount);
  });
});
