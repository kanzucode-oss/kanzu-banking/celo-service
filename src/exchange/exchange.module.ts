import { Module, HttpModule } from '@nestjs/common';
import { ExchangeService } from './exchange.service';
import { ExchangeController } from './exchange.controller';

@Module({
  imports:[HttpModule],
  providers: [ExchangeService],
  controllers: [ExchangeController]
})
export class ExchangeModule {}
