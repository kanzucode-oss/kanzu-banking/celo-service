import {HttpService, Injectable} from '@nestjs/common';
import {ExchangeDto} from './exchange.dto';

@Injectable()
export class ExchangeService {
    constructor(private http: HttpService) {
    }

    async exchange(amount: number, from: string, to: string,): Promise<ExchangeDto> {
        const url = `https://api.exchangerate.host/latest?base=UGX&symbols=USD`;
        const url2 = `https://api.nomics.com/v1/currencies/ticker?key=${process.env.NOMICS_API_KEY}&ids=CUSD3&convert=USD`
        const convertUGX = await this.http.get(url).toPromise();
        const cUSD = await this.http.get(url2).toPromise();
        if (from == 'UGX' && to == 'USD') {
            const rate = convertUGX.data.rates.USD;
            const convertAmount = amount * rate;
            const [data] = cUSD.data;
            const price = data.price;
            const cusdAmount = parseFloat((convertAmount / price).toFixed(5));
            return {
                from: 'UGX',
                to: 'USD',
                amount: cusdAmount
            }
        }
        if (from == 'USD' && to == 'UGX') {

            const [data] = cUSD.data;
            const price = data.price;
            const rate = convertUGX.data.rates.USD;
            const usdAmount = parseFloat((amount * price).toFixed(5));
            const ugxAmount = Math.floor(usdAmount / rate);
            return {
                from: 'USD',
                to: 'UGX',
                amount: ugxAmount
            }
        }
    }
}
