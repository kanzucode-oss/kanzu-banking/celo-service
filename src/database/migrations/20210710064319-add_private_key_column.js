/* eslint-disable no-undef */
'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('FSPs', 'private_key', {
        type: Sequelize.STRING,
        allowNull: false,
      }),
    ]);
  },

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  down: async (queryInterface, Sequelize) => {
    return Promise.all([queryInterface.removeColumn('FSPs', 'private_key')]);
  }
};
