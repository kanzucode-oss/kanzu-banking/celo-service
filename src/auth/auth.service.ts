// process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'; // Using this to ignore SSL certificate issues
import {Injectable, HttpService, Request, BadRequestException} from '@nestjs/common';

@Injectable()
export class AuthService {
    constructor(private http: HttpService) {
    }

    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    async validateUser(@Request() req): Promise<boolean> {
        const fsp = req.get('Fineract-Platform-TenantId');
        const auth_key = req.get('Authorization')
        if (fsp && auth_key) {
            // header looks like 'Bearer <token>', so we skip 7 characters out of the string
            return process.env.AUTH_KEY === auth_key.substring(7)
        } else {
            throw new BadRequestException("Missing Tenant Identifier");
        }

    }


}
