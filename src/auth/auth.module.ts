import { Module, HttpModule } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { BasicTokenStrategy } from './token-auth-strategy';

@Module({
  imports:[PassportModule, HttpModule],
  providers: [AuthService, BasicTokenStrategy]
})
export class AuthModule {}
