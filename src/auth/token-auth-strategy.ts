import {Strategy} from 'passport-http-bearer';
import {Injectable, UnauthorizedException} from '@nestjs/common';
import {PassportStrategy} from '@nestjs/passport';
import {AuthService} from './auth.service';

@Injectable()
export class BasicTokenStrategy extends PassportStrategy(Strategy) {
    constructor(
        private authService: AuthService,
    ) {
        super({
            passReqToCallback: true
        });
    }

    // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
    public validate = async (token: string): Promise<boolean> => {
        const user = await this.authService.validateUser(token)
        if (!user) {
            throw new UnauthorizedException()
        }
        return user
    }
}
